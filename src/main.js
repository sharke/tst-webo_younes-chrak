import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import { BootstrapVue } from "bootstrap-vue";
import moment from "vue-moment";
import "bootstrap-vue/dist/bootstrap-vue.css";

// install bootstrapVue
Vue.use(BootstrapVue);

Vue.use(moment);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
